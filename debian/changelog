neutron-dynamic-routing (2:25.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090445).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 13:08:42 +0100

neutron-dynamic-routing (2:25.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Oct 2024 08:29:26 +0200

neutron-dynamic-routing (2:25.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 21 Sep 2024 14:17:13 +0200

neutron-dynamic-routing (2:25.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Sep 2024 09:34:06 +0200

neutron-dynamic-routing (2:24.0.0-3) unstable; urgency=medium

  * Drop the build-dependency on python3-mock (Closes: #1071012).

 -- Thomas Goirand <zigo@debian.org>  Mon, 13 May 2024 09:05:38 +0200

neutron-dynamic-routing (2:24.0.0-2) unstable; urgency=medium

  * Restrict tests to Architecture: amd64 arm64 ppc64el.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 May 2024 16:31:27 +0200

neutron-dynamic-routing (2:24.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 22:23:13 +0200

neutron-dynamic-routing (2:24.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Depends on neutron 24.0.0~.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Mar 2024 11:35:28 +0100

neutron-dynamic-routing (2:23.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 13:00:11 +0200

neutron-dynamic-routing (2:23.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 13:53:05 +0200

neutron-dynamic-routing (2:22.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 10:18:20 +0200

neutron-dynamic-routing (2:22.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 16:19:07 +0100

neutron-dynamic-routing (2:22.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bookworm.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Mar 2023 09:17:54 +0100

neutron-dynamic-routing (2:21.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 06 Oct 2022 08:47:33 +0200

neutron-dynamic-routing (2:21.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Sep 2022 17:51:14 +0200

neutron-dynamic-routing (2:21.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Sep 2022 12:41:29 +0200

neutron-dynamic-routing (2:20.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 21:31:54 +0200

neutron-dynamic-routing (2:20.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 25 Mar 2022 15:45:27 +0100

neutron-dynamic-routing (2:20.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Add autopkgtest.
  * Add pyroute2 as build-depends.
  * Use neutron-lib >= 2.20.0, neutron 2:20.0.0~ and pyroute2 (>= 0.6.7).

 -- Thomas Goirand <zigo@debian.org>  Thu, 10 Mar 2022 15:37:14 +0100

neutron-dynamic-routing (2:19.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 11 Jan 2022 11:40:14 +0100

neutron-dynamic-routing (2:19.0.0-2) unstable; urgency=medium

  * Added es.po Spanish translation of the debconf template thanks to Camaleón
    (Closes: #987709).
  * Added pt_BR.po Brazilian Portuguese translation of the debconf template
    thanks to Paulo Henrique de Lima Santana (Closes: #986497).

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Dec 2021 12:20:04 +0100

neutron-dynamic-routing (2:19.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:37:36 +0200

neutron-dynamic-routing (2:19.0.0~rc2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Oct 2021 00:55:41 +0200

neutron-dynamic-routing (2:19.0.0~rc2-1) experimental; urgency=medium

  * New upstream release.
  * Removed fix-TypeError-when-formatting-BGP-IP-address.patch applied
    upstream.
  * Fixed min version of python3-neutron-lib and python3-neutron.

 -- Thomas Goirand <zigo@debian.org>  Fri, 17 Sep 2021 11:05:13 +0200

neutron-dynamic-routing (2:18.0.0-3) unstable; urgency=medium

  * Upload to unstable.

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 16 Aug 2021 16:29:42 +0200

neutron-dynamic-routing (2:18.0.0-2) experimental; urgency=medium

  * d/patches: Add fix-TypeError-when-formatting-BGP-IP-address.patch

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 15 Jul 2021 14:29:39 +0200

neutron-dynamic-routing (2:18.0.0-1) experimental; urgency=medium

  * Set python3-neutron (>= 18.0.0~).
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Apr 2021 14:13:01 +0200

neutron-dynamic-routing (2:18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Removed (build-)depends versions when satisfied in Bullseye.
  * Standards-Version: 4.5.1.
  * Refreshed Fix_outerjoin_to_comply_with_SQLAlchemy_1.3_strictness.patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 08:40:44 +0100

neutron-dynamic-routing (2:17.0.0-2) unstable; urgency=medium

  * Add upstream patch for SQLA 1.3:
    - Fix_outerjoin_to_comply_with_SQLAlchemy_1.3_strictness.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Dec 2020 17:31:40 +0100

neutron-dynamic-routing (2:17.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2020 15:14:21 +0200

neutron-dynamic-routing (2:17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2020 21:06:43 +0200

neutron-dynamic-routing (2:16.0.0-2) unstable; urgency=medium

  * Add add-retry-of-get_bgp_speakers.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Aug 2020 13:11:01 +0200

neutron-dynamic-routing (2:16.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 18:09:05 +0200

neutron-dynamic-routing (2:16.0.0~rc2-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 09 May 2020 21:46:46 +0200

neutron-dynamic-routing (2:16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Move the package to the neutron-plugins subgroup.

 -- Thomas Goirand <zigo@debian.org>  Thu, 23 Apr 2020 09:41:54 +0200

neutron-dynamic-routing (2:15.0.0-3) unstable; urgency=medium

  * Correctly detect IP address when using bgptothehost (ie: no physical NIC
    has a valid IPv4): fallback to hostname -i.

 -- Thomas Goirand <zigo@debian.org>  Fri, 13 Mar 2020 15:55:52 +0100

neutron-dynamic-routing (2:15.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 22 Oct 2019 23:13:18 +0200

neutron-dynamic-routing (2:15.0.0-1) experimental; urgency=medium

  * Added debconf translations of templates:
    - fr.po (Closes: #926837).
    - nl.po (Closes: #926662).
    - pt.po (Closes: #924887).
    - de.po (Closes: #924781).
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 20:38:14 +0200

neutron-dynamic-routing (2:15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 03 Oct 2019 17:16:34 +0200

neutron-dynamic-routing (2:14.0.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sat, 20 Jul 2019 17:51:04 +0200

neutron-dynamic-routing (2:14.0.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Removed package versions when satisfied in Buster.
  * Fixed (build-)Depends for this release.
  * Standards-Version: 4.3.0 (no change).
  * Generating the policy file with oslopolicy-sample-generator.

  [ Michal Arbet ]
  * Change driver ryu to os-ken driver

 -- Michal Arbet <michal.arbet@ultimum.io>  Tue, 25 Jun 2019 09:40:51 +0200

neutron-dynamic-routing (2:13.0.0-2) unstable; urgency=medium

  * d/control: Add me to uploaders field
  * d/neutron-bgp-dragent.init.in:
    - Add agent.conf.d configdir as it is in other neutron's plugins
  * d/rules: Fix default config
  * Fix db migration if user set config via debconf (Closes: #924476).
  * Router id is now configurable via debconf
  * Nothing changed if used non-interactive-mode

 -- Michal Arbet <michal.arbet@ultimum.io>  Wed, 13 Mar 2019 11:54:44 +0100

neutron-dynamic-routing (2:13.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Sep 2018 20:29:38 +0200

neutron-dynamic-routing (2:13.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Aug 2018 21:28:28 +0200

neutron-dynamic-routing (2:12.0.0-3) unstable; urgency=medium

  * Fixed python 3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Mar 2018 18:45:02 +0000

neutron-dynamic-routing (2:12.0.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: Remove Python 2.7 in description (Closes: #891949).

 -- David Rabel <david.rabel@noresoft.com>  Fri, 09 Mar 2018 17:27:46 +0100

neutron-dynamic-routing (2:12.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Switch to Python 3.
  * Add neutron-common as build-depends, since the unit tests are attempting
    to read neutron.conf.
  * Blacklist TestBgpDrAgent.test_bgp_dragent_manager().
  * Standards-Version is now 4.1.3.
  * Removed dh-systemd build-depends.

 -- Thomas Goirand <zigo@debian.org>  Thu, 01 Mar 2018 20:55:34 +0100

neutron-dynamic-routing (2:11.0.0-1) unstable; urgency=medium

  * New upstream release:
    - Fixes FTBFS (Closes: #882534).
  * Fixed (build-)depends for this release, and ran wrap-and-sort -bast.
  * Updated maintainer.
  * d/copyright: https for the format field.
  * Updated VCS URLs.
  * Deprecated priority extra, as per policy 4.1.0.
  * Standards-Version is now 4.1.2.
  * Removes requirements patch.

 -- Thomas Goirand <zigo@debian.org>  Thu, 07 Dec 2017 17:30:07 +0100

neutron-dynamic-routing (2:9.0.0-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Patch requirements.txt to drop SQLAlchemy << 1.1 dependency

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 23 Jan 2017 23:02:04 +0100

neutron-dynamic-routing (2:9.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Oct 2016 07:08:54 +0000

neutron-dynamic-routing (2:9.0.0~rc2-1) experimental; urgency=medium

  * Initial release (Closes: #839826).

 -- Thomas Goirand <zigo@debian.org>  Sat, 16 Jul 2016 13:48:56 +0200
